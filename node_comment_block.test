<?php

/**
 * @file
 * Tests for node_comment_block.module
 */

/**
 * Functional tests for node_comment_block.
 */
class NodeCommentBlockWebTest extends DrupalWebTestCase {

  /**
   * The path to POST to, to configure the block.
   */
  const BLOCK_CONFIGURE_PATH = 'admin/structure/block/manage/node_comment_block/node_comments/configure';

  /**
   * The block HTML to assert for.
   */
  const COMMENT_BLOCK_HTML = '<div id="block-node-comment-block-node-comments" class="block block-node-comment-block">';

  /**
   * Implements getInfo().
   */
  const COMMENT_BLOCK_SUBJECT = 'Add new comment';

  public static function getInfo() {
    return array(
      'name' => 'Node Comment Block web tests',
      'description' => 'Tests the functionality of Node Comment Block module.',
      'group' => 'Node Comment Block',
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    return parent::setUp(array('comment', 'node_comment_block'));
  }

  /**
   * Test displaying the comments block for authenticated users.
   */
  public function testAuthenticatedUsers() {
    $this->drupalLogin($this->drupalCreateUser(array(
      'administer blocks',
      'post comments',
    )));

    $this->drupalCreateNode(array('type' => 'article'));

    // The comments form should be hidden by default with the module enabled.
    $this->drupalGet('node/1');
    $this->assertNoText(self::COMMENT_BLOCK_SUBJECT, 'The comment block is not visible.');

    // Place the node comments block into a region.
    $this->drupalPost(
      self::BLOCK_CONFIGURE_PATH,
      array('regions[bartik]' => 'sidebar_second'),
      t('Save block')
    );

    // The comments form should now be visible in the block.
    $this->drupalGet('node/1');
    $this->assertRaw(self::COMMENT_BLOCK_HTML, 'The comment block is visible.');
    $this->assertText(self::COMMENT_BLOCK_SUBJECT, 'The comment block subject is visible.');
  }

  /**
   * Test showing a block to anonymous users.
   */
  public function testAnonymousUsers() {
    user_role_change_permissions(DRUPAL_ANONYMOUS_RID, array(
      'access comments' => TRUE,
      'post comments' => FALSE,
    ));

    $account = $this->drupalCreateUser(array(
      'administer blocks',
      'create article content'
    ));
    $this->drupalLogin($account);

    // Place the node comments block into a region.
    $this->drupalPost(
      self::BLOCK_CONFIGURE_PATH,
      array('regions[bartik]' => 'sidebar_second'),
      t('Save block')
    );

    $this->drupalCreateNode(array('type' => 'article'));
    $this->drupalLogout();

    $this->drupalGet('node/1');
    $this->assertNoRaw(self::COMMENT_BLOCK_HTML, 'The comments block is not visible.');

    // Show the block to anonymous users.
    $this->drupalLogin($account);
    $this->drupalPost(
      self::BLOCK_CONFIGURE_PATH,
      array('regions[bartik]' => 'sidebar_second', 'show_anonymous' => TRUE),
      t('Save block')
    );
    $this->drupalLogout();

    $this->drupalGet('node/1');
    $this->assertRaw(self::COMMENT_BLOCK_HTML, 'The comments block is visible.');
  }

}
