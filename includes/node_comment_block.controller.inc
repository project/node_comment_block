<?php

/**
 * @file
 * A controller class for Node Comment Block.
 */

/**
 * Class NodeCommentBlock
 */
class NodeCommentBlock {

  /**
   * Get the node ID from a path.
   *
   * @param string $path
   *   The path.
   *
   * @return \stdClass|null
   *   The node or NULL.
   */
  public static function getNode($path) {
    preg_match('/node\/(\d+)\/?$/', $path, $matches);

    return isset($matches[1]) ? node_load($matches[1]) : NULL;
  }

  /**
   * Store the comments for a node.
   *
   * @param \stdClass $node
   *   The node.
   */
  public static function setComments(\stdClass $node) {
    if (!array_key_exists('comments', $node->content)) {
      return;
    }

    global $_node_comment_block_comments;
    $_node_comment_block_comments[$node->nid] = $node->content['comments'];
  }

  /**
   * Get the comments for a node.
   *
   * @param \stdClass $node
   *   The node.
   *
   * @return array
   *   An array of comment data.
   */
  public static function getComments(\stdClass $node) {
    global $_node_comment_block_comments;

    if ($_node_comment_block_comments === NULL) {
      return array();
    }

    if (!array_key_exists($node->nid, $_node_comment_block_comments)) {
      return array();
    }

    return $_node_comment_block_comments[$node->nid];
  }

}
